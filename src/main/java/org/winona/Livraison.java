package org.winona;

import org.winona.enums.TypeFoin;

import java.time.LocalDate;
import java.util.List;

public class Livraison {
    private LotFoin lotFoin;
    private LocalDate dateLivraison;
    private Salarie salarie;

    //___CONSTRUCTOR___//
    public Livraison() {
    }

    public Livraison(TypeFoin typeFoin, String numeroLot, Integer quantiteLot) {
        this.lotFoin = new LotFoin(numeroLot, quantiteLot, typeFoin);
    }

    //___METHOD___//

    /**
     * Cette méthode permet d'assigner un salarié à une réception. L'attribut date sera la date du jour.
     * @param salarie Salarie
     */
    public void receptionnerFoin(Salarie salarie) {
        this.salarie = salarie;
        this.dateLivraison = LocalDate.now();
    }

    /**
     * Cette méthode permet de retourner la date de livraison d'un lot donné.
     * @param livraisons Livraison
     * @param numLot String
     * @return Date
     */
    public static LocalDate dateLivraisonDuLot(List<Livraison> livraisons, String numLot) {
        for (Livraison i : livraisons) {
            if (numLot != null)
                System.out.println(i.getDateLivraison());
            return i.getDateLivraison();
        }
        return null;
    }

    //___GETTER___//

    public LocalDate getDateLivraison() {
        return dateLivraison;
    }

    public Salarie getSalarie() {
        return salarie;
    }

    public LotFoin getLotFoin() {
        return lotFoin;
    }

    //___SETTER___//

    public void setSalarie(Salarie salarie) {
        this.salarie = salarie;
    }
}
