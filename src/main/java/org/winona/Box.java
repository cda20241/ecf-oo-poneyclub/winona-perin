package org.winona;

import java.util.ArrayList;
import java.util.List;

public class Box {
    private char nom;
    private Integer taille;
    private Integer quantiteFoinCourrante;
    private Integer quantiteFoinMax;
    private List<Poney> poneys = new ArrayList<>();

    //___CONSTRUCTOR___//

    public Box() {
    }

    public Box(char nom, Integer taille, Integer quantiteFoinCourrante, Integer quantiteFoinMax) {
        this.nom = nom;
        this.taille = taille;
        this.quantiteFoinCourrante = quantiteFoinCourrante;
        this.quantiteFoinMax = quantiteFoinMax;
    }

    //___GETTER___//

    public char getNom() {
        return nom;
    }

    public Integer getTaille() {
        return taille;
    }

    public Integer getQuantiteFoinCourrante() {
        return quantiteFoinCourrante;
    }

    public Integer getQuantiteFoinMax() {
        return quantiteFoinMax;
    }

    public List<Poney> getPoneys() {
        return poneys;
    }

    //___SETTER___//

    public void setNom(char nom) {
        this.nom = nom;
    }

    public float setQuantiteFoinCourrante(Integer quantiteFoinCourrante) {
        this.quantiteFoinCourrante = quantiteFoinCourrante;
        return 0;
    }

    public void setPoneys(List<Poney> poneys) {
        this.poneys = poneys;
    }
}
