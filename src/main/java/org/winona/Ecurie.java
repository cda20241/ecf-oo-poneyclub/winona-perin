package org.winona;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ecurie {
    private String nom;
    private List<Salarie> salaries = new ArrayList<>();
    private List<Poney> poneys = new ArrayList<>();
    private List<Box> boxes = new ArrayList<>();
    private List<Livraison> livraisons = new ArrayList<>();

    //___CONSTRUCTOR___//

    public Ecurie() {
    }
    public Ecurie(String nom) {
        this.nom = nom;
    }

    //___METHOD___//

    /**
     * Cette méthode permet de répartir le foin dans les boxes de façon à ce qu'ils aient le même pourcentage.
     * @param lotRepartir LotFoin
     * @return HashMap(Box nom, Int quantité de foin)
     */
    public Map repartirFoin(LotFoin lotRepartir) {

        //ici je calcule de combien je dispose de foin en tout boxes et livraison//

        float foinTotalDisponible;

        foinTotalDisponible = lotRepartir.getQuantiteLot().floatValue();

        for (Box b : boxes) {
            foinTotalDisponible += b.getQuantiteFoinCourrante().floatValue();
        }
        System.out.println("Foin total disponible : " + foinTotalDisponible);

        //calcul des quantités de foin pour chaque box, partage de ratio//

        float capaciteTotaleBoxes = 1;

        for (Box b : boxes) {
            capaciteTotaleBoxes += b.getQuantiteFoinMax();
        }

        float quantiteFoin = 0;

        for (Box b : boxes) {
            try {
                quantiteFoin = b.getQuantiteFoinMax()*(foinTotalDisponible/capaciteTotaleBoxes);
                b.setQuantiteFoinCourrante((int) quantiteFoin);
            } catch (ArithmeticException e) {
                System.out.println("Vérifiez que la capacité totale des boxes n'est pas nulle.");
            }
        }

        //on en fait une HashMap//

        Map<Box, Integer> boxEtQuantite = new HashMap<>();

        for (Box b : boxes) {
            boxEtQuantite.put(b, b.getQuantiteFoinCourrante());
            System.out.println(b.getQuantiteFoinCourrante());
        }
        return boxEtQuantite;
    }

    /**
     * Cette méthode ajoute une personne aux salariés de l'écurie.
     * @param personne Personne
     * @param fonction String
     */
    public void recruter(Personne personne, String fonction){
        Salarie salarie = new Salarie(personne, fonction, this);
        this.salaries.add(salarie);
        salarie.setEcurie(this);

        System.out.println("Vous venez de recruter " + salarie.getPrenom() + " " + salarie.getNom());
    }

    /**
     * Cette méthode supprime un Salarié de l'écurie.
     * @param salarie Salarie
     */
    public void  licencier(Salarie salarie) {
        this.salaries.remove(salarie);
        salarie.setEcurie(null);

        System.out.println("Vous venez de licencier " + salarie.getPrenom() + " " + salarie.getNom());
    }

    /**
     * Cette méthode permet d'ajouter un poney de la liste des poneys.
     * @param poney Poney
     */
    public void acheterPoney(Poney poney){
        this.poneys.add(poney);
        poney.setEcurie(this);

        System.out.println("Vous venez d'acquérir le poney " + poney.getNom());
    }

    /**
     * Cette méthode permet de supprimer un poney de la liste des poneys.
     * @param poney Poney
     */
    public void vendrePoney(Poney poney){
        this.poneys.remove(poney);
        poney.setEcurie(null);

        System.out.println("Vous venez de vous séparer du poney " + poney.getNom());
    }

    /**
     * Cette méthode permet de sortir un poney de son box.
     * @param poney Poney
     */
    public void envoyerPaitre(Poney poney){
        poney.getBox().getPoneys().remove(poney);
        poney.setBox(null);
        System.out.println(poney.getNom() +" est parti paître.");
    }

    /**
     * Cette méthode permet de vérifier si le poney est dans un box, si non, il le rentre dans le box donné.
     * @param poney Poney
     * @param box Box
     */
    public void rentrerPoney(Poney poney, Box box) {

            if (poney.getBox() == null) {
                poney.setBox(box);
                box.getPoneys().add(poney);
                System.out.println(poney.getNom() + " est rentré dans le box " + box.getNom());
            } else if (poney.getBox() == box) {
                System.out.println(poney.getNom() + " est encore dans ce box.");
            } else {
                System.out.println("Le poney n'est pas sorti de son précédent box, veuillez l'envoyer paître.");
            }
    }

    /**
     * Cette méthode permet d'afficher le remplissage de tous les boxes en % de foin.
     */
    public void afficherRemplissageDesBoxes(){
        for (Box b : boxes) {
            float foinActuel = b.getQuantiteFoinCourrante().floatValue();
            float foinMax = b.getQuantiteFoinMax().floatValue();
            float remplissage = (foinActuel/foinMax)*100;
            System.out.println(b.getNom() + " " + Math.round(remplissage) + "%");
        }
    }

    //___GETTER___//

    public String getNom() {
        return nom;
    }

    public List<Salarie> getSalaries() {
        return salaries;
    }

    public List<Poney> getPoneys() {
        return poneys;
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public List<Livraison> getLivraisons() {
        return livraisons;
    }

    //___SETTER___//

    public void setSalaries(List<Salarie> salaries) {
        this.salaries = salaries;
    }

    public void setPoneys(List<Poney> poneys) {
        this.poneys = poneys;
    }

    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }

    public void setLivraisons(List<Livraison> livraisons) {
        this.livraisons = livraisons;
    }
}
