package org.winona;

public class Salarie extends Personne{
    private String fonction;
    private Ecurie ecurie;

    //___CONSTRUCTOR___//
    public Salarie() {
    }

    public Salarie(Personne personne, String fonction, Ecurie ecurie) {
        this.setNom(personne.getNom());
        this.setPrenom(personne.getPrenom());
        this.fonction = fonction;
        this.ecurie = ecurie;
    }
    //___GETTER___//

    public String getFonction() {
        return fonction;
    }

    public Ecurie getEcurie() {
        return ecurie;
    }

    //___SETTER___//

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public void setEcurie(Ecurie ecurie) {
        this.ecurie = ecurie;
    }
}
