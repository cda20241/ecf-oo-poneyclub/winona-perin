package org.winona;

import org.winona.enums.TextureCriniere;

public class Criniere {
    private Integer longueur;
    private TextureCriniere texture;

    //___CONSTRUCTOR___//

    public Criniere() {
    }
    public Criniere(Integer longueur, TextureCriniere texture) {
        this.longueur = longueur;
        this.texture = texture;
    }

    //___GETTER___//

    public Integer getLongueur() {
        return longueur;
    }

    public TextureCriniere getTexture() {
        return texture;
    }

    //___SETTER___//

    public void setLongueur(Integer longueur) {
        this.longueur = longueur;
    }

    public void setTexture(TextureCriniere texture) {
        this.texture = texture;
    }
}
