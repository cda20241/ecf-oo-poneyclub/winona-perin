package org.winona.enums;

public enum TextureCriniere {
    LISSE("lisse"),
    FRISEE("frisée");

    private String type;

    TextureCriniere(String type) {
        this.type = type;
    }
}
