package org.winona;

public class Personne {
    private String nom;
    private String prenom;

    //___CONSTRUCTOR___//
    public Personne() {
    }
    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    //___METHOD___//

    //___GETTER___//

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }


    //___SETTER___//

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
