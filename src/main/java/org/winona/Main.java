package org.winona;

import org.winona.enums.TextureCriniere;
import org.winona.enums.TypeFoin;

public class Main {
    public static void main(String[] args) {

        //init//

        Personne oKenobi = new Personne("KENOBI", "Obi-wan");
        Personne lSkywalker = new Personne("SKYWALKER", "Luc");

        Poney poneyUsainBolt = new Poney("Usain Bolt", 250, new Criniere(25, TextureCriniere.LISSE));
        Poney poneyPetitTonnerre = new Poney("Petit Tonnerre", 280, new Criniere(28, TextureCriniere.FRISEE));
        Poney poneyLeFeu = new Poney("Le Feu", 200, new Criniere(30, TextureCriniere.LISSE));
        Poney poneyLuisMariano = new Poney("Luis Mariano", 245, new Criniere(20, TextureCriniere.FRISEE));


        Ecurie tatooinePoneyClub = new Ecurie("Tatooine Poney Club");

        Box boxA = new Box('A', 3, 200, 600);
        Box boxB = new Box('B', 4, 300, 600);
        Box boxC = new Box('C', 5, 900, 1000);
        Box boxD = new Box('D', 6, 100, 800);

        tatooinePoneyClub.getBoxes().add(boxA);
        tatooinePoneyClub.getBoxes().add(boxB);
        tatooinePoneyClub.getBoxes().add(boxC);
        tatooinePoneyClub.getBoxes().add(boxD);

        //fin//

        System.out.println("\n//___EMBAUCHER___//\n");

        tatooinePoneyClub.recruter(oKenobi, "salarié");
        tatooinePoneyClub.recruter(lSkywalker, "salarié");

        tatooinePoneyClub.acheterPoney(poneyUsainBolt);
        tatooinePoneyClub.acheterPoney(poneyPetitTonnerre);
        tatooinePoneyClub.acheterPoney(poneyLeFeu);
        tatooinePoneyClub.acheterPoney(poneyLuisMariano);

        System.out.println("\n//___GESTION DES PONEYS___//\n");

        tatooinePoneyClub.rentrerPoney(poneyUsainBolt, boxA);
        tatooinePoneyClub.rentrerPoney(poneyPetitTonnerre, boxA);
        tatooinePoneyClub.rentrerPoney(poneyLeFeu, boxB);
        tatooinePoneyClub.rentrerPoney(poneyLuisMariano, boxC);

        tatooinePoneyClub.envoyerPaitre(poneyUsainBolt);


        System.out.println("\n//___LIVRAISONS___//\n");

        Livraison livraison1 = new Livraison(TypeFoin.FOINPRAIRIE, "F003", 200);

        livraison1.receptionnerFoin(tatooinePoneyClub.getSalaries().get(1));

        tatooinePoneyClub.getLivraisons().add(livraison1);

        Livraison.dateLivraisonDuLot(tatooinePoneyClub.getLivraisons(), "F003");


        System.out.println("\n//___REMPLISSAGE DES BOXES___//\n");

        tatooinePoneyClub.afficherRemplissageDesBoxes();

        tatooinePoneyClub.repartirFoin(livraison1.getLotFoin());

        tatooinePoneyClub.afficherRemplissageDesBoxes();

    }
}