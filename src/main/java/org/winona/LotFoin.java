package org.winona;

import org.winona.enums.TypeFoin;

public class LotFoin {
    private String numeroLot;
    private TypeFoin typeFoin;
    private Integer quantiteLot;

    //___CONSTRUCTOR___//
    public LotFoin() {
    }

    public LotFoin(String numeroLot, Integer quantiteLot, TypeFoin typeFoin) {
        this.numeroLot = numeroLot;
        this.typeFoin = typeFoin;
        this.quantiteLot = quantiteLot;
    }

    //___GETTER___//

    public String getNumeroLot() {
        return numeroLot;
    }

    public TypeFoin getTypeFoin() {
        return typeFoin;
    }

    public Integer getQuantiteLot() {
        return quantiteLot;
    }

    //___SETTER___//

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public void setTypeFoin(TypeFoin typeFoin) {
        this.typeFoin = typeFoin;
    }

    public void setQuantiteLot(Integer quantiteLot) {
        this.quantiteLot = quantiteLot;
    }
}
